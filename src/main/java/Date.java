import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Date {
        private double pipeInDiameter=147;  //管道外直径mm
        private double flowRate=0.02;    //流量m^3/s
        private double pipeThickness=6;  //厚度6mm
        private double[][] pumpParameter = {{15,672}, {25,600}, {30,552}};  //泵输水时的参数 Q-H   m^3/s-m
        private double oilViscosity=3.34E-6;   //油品粘度: m^2/s (柴油)
        public final static double PI=3.14;    //圆周率
        private double roughness=0.1;     //管壁粗糙度
        private double relativeRoughness=0.001361;    //相对粗糙度
        private double re;         //雷诺数
        public double m;          //列宾宗参数

        public double reynoldsNumber(double flowRate,double pipeInDiameter,double oilViscosity ) {   //得到雷诺数
            re = 4 * flowRate / (PI * pipeInDiameter/1000 * oilViscosity);
            return re;
        }

        public double flowPattern(double reynolds) {               //得到列宾宗参数
            if (reynolds < 2000) {       //层流区
                m=1;
            }else if(reynolds<59.7/(Math.pow(0.001361,(double)8/7))){     //水力光滑区   两个整型相除得到的是一个整数，需强制转换
                m=0.25;
            } else if (reynolds < (665 - 765 * Math.log10(0.001361)) / 0.001361) {     //混合摩擦区
                m = 0.123;
            } else {                             //完全粗糙区
                m=0;
            }
            return m;
        }

        public double[][] getPumpParameter(double[][] pumpParameterList) {
            for (int i = 0; i < 3; i++) {
            pumpParameterList[i][0]=Math.pow(pumpParameterList[i][0],2-m);
        }
        return pumpParameterList;
    }

        public double[] getLinearFitting(double[][] pumpParameter ) {
            SimpleRegression regression = new SimpleRegression();
            regression.addData(pumpParameter);

            RegressionResults results = regression.regress();
            double b = results.getParameterEstimate(0);
            double k = results.getParameterEstimate(1);
            double r2 = results.getRSquared();
            return new double[]{b, k};

        }

        public static void main(String[] args) {
            Date date = new Date();
            date.reynoldsNumber(date.flowRate, date.pipeInDiameter, date.oilViscosity);
            date.flowPattern(date.re);      //得出m的值
            double [][] s=date.getPumpParameter(date.pumpParameter);
            double [] xy=date.getLinearFitting(s);
            System.out.println("得到的拟合曲线为");
            System.out.print("H=" + xy[0] + xy[1]+"Q^" + (2 - date.m));
            Scanner str = new Scanner(System.in);
            while (true) {
                System.out.println("\n请输入流量：(m^3/s)   输入0退出");
                double Q=str.nextDouble();
                if (Q == 0) {
                    break;
                }
                System.out.println("H=" + (xy[0] + xy[1] * Math.pow(Q, 2 - date.m)));
            }
        }
}

